using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class  PlayerController : MonoBehaviour
{

    private Vector2 temp, temp2;
    private bool star = false;

    public GameObject rightBullet;
    public GameObject leftBullet;

    public List<AudioClip> AudioClips;

    private int contador = 0;
    private bool puedeSaltar = false;

    private int speed2 = 20;
    private bool puedeSubirEscalera = false;

    private SpriteRenderer sr;
    private Animator _animator;
    private Rigidbody2D rb2d;
    private BoxCollider2D col;
    private AudioSource audioSource;

    //public Text lifeText;
    //public Text scoreText;
    private int Score = 0;
    private int Life = 3;
    private bool esIntangible = false;
    private float maxIntangibleTime = 1f;
    private float intangibleTime = 0;

    private float switchColorDelay = 0.1f;
    private float switchColorTime = 0f;
    private Color originalColor;

    private float maxPowerTime = 7f;
    private float powerTime = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        col = GetComponent<BoxCollider2D>();
        rb2d = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
        originalColor = sr.color;
    }

    // Update is called once per frame
    void Update()
    {

        //lifeText.text = "VIDA: " + Life;
        //scoreText.text = "PUNTAJE: " + Score;

        if(Input.GetKeyDown(KeyCode.A))
        {
            if(!sr.flipX)
            {
                var position = new Vector2(transform.position.x + 1, transform.position.y);
                Instantiate(rightBullet, position, rightBullet.transform.rotation);
            }
            else
            {
                var position = new Vector2(transform.position.x - 2, transform.position.y);
                Instantiate(leftBullet, position, leftBullet.transform.rotation);
            }
            audioSource.PlayOneShot(AudioClips[1]);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            sr.flipX= false;
            setRunAnimation();
            rb2d.velocity = new Vector2(15,rb2d.velocity.y);
        }
        else
        {
            setIdleAnimation();
            rb2d.velocity = new Vector2(0,rb2d.velocity.y);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            sr.flipX = true;
            setRunAnimation();
            rb2d.velocity = new Vector2(-15,rb2d.velocity.y);
        }

        if (Input.GetKey(KeyCode.DownArrow) && puedeSaltar == false)
        {
            setSlideAnimation();
        }

        if (Input.GetKeyDown(KeyCode.Space) && puedeSaltar)
        {
            audioSource.PlayOneShot(AudioClips[0]);
            float upSpeed = 40;
            setJumpAnimation();
            rb2d.velocity = Vector2.up * upSpeed;
            if(contador != 1 && puedeSaltar == true){
                setJumpAnimation();
                audioSource.PlayOneShot(AudioClips[0]);
                puedeSaltar = true;
                contador ++;
            }
            else{
                puedeSaltar = false;
            }
            //rb2d.velocity = new Vector2(rb2d.velocity.x,5);
            //rb2d.AddForce(new Vector2(0,100));
        }

        //Debug.Log("Mensaje por cada frame " + sr.flipX);

        if (puedeSubirEscalera && Input.GetKey(KeyCode.UpArrow))
        {
            //col.enabled = false;
            rb2d.gravityScale = 0;
            rb2d.velocity = Vector2.up * speed2;
            DeshabilitarColisionConSuelo();
        }

        if (puedeSubirEscalera && Input.GetKey(KeyCode.DownArrow))
        {
            rb2d.gravityScale = 10;
            //col.enabled = true;
            rb2d.velocity = Vector2.down * speed2;
            DeshabilitarColisionConSuelo();
        }

        if (esIntangible && intangibleTime < maxIntangibleTime)
        {
            intangibleTime += Time.deltaTime;
            Parpadear();
            DeshabilitarColisionConEnemigo();
        }

        if (intangibleTime >= maxIntangibleTime)
        {
            HabilitarColisionConEnemigo();
            intangibleTime = 0;
            esIntangible = false;
            sr.enabled = true;
        }

        if (star == true && powerTime < maxPowerTime)
        {
            Inmortal();
            //audioSource.Pause();
            // audioSource.PlayOneShot(AudioClips[6]);
        }

        if (powerTime >= maxPowerTime)
        {
            Mortal();
        }
        if (Life == 0)
        {
            Morir();
        }
    }


    private void Mortal()
    {
        sr.color = originalColor;
        Decrecer();
        this.gameObject.layer = 6;
    }

    private void Inmortal()
    {
        SwitchColor();
        powerTime += Time.deltaTime;
        this.gameObject.layer = 9;
    }

    private void Crecer()
    {
        audioSource.PlayOneShot(AudioClips[5]);
        if(star == true)
        {
            temp = transform.localScale;
            temp.x += .5f;
            temp.y += .5f;
            transform.localScale = temp;
        }
    }

    private void Decrecer()
    {
        if(star == true)
        {
            temp = transform.localScale;
            temp.x -= .5f;
            temp.y -= .5f;
            transform.localScale = temp;
        }
        star = false;
    }

    private void Morir()
    {
        if(Life == 0)
        {
            setDeadAnimation();
        }
    }

    private void SwitchColor()
    {
        if(sr.color == originalColor)
            sr.color = Color.yellow;
        else
            sr.color = originalColor;
        switchColorTime = 0;
    }

    public void DeshabilitarColisionConEnemigo()
    {
        Physics2D.IgnoreLayerCollision(6,3,true);
    }

    public void HabilitarColisionConEnemigo()
    {
        Physics2D.IgnoreLayerCollision(6,3,false);
    }

    public void DeshabilitarColisionConSuelo()
    {
        Physics2D.IgnoreLayerCollision(6,7,true);
    }

    public void HabilitarColisionConSuelo()
    {
        Physics2D.IgnoreLayerCollision(6,7,false);
    }

    private void Parpadear()
    {
        sr.enabled = !sr.enabled;
    }
    
    void OnTriggerStay2D(Collider2D other) {
        if(other.gameObject.tag == "Stair")
        {
            puedeSubirEscalera = true;
            rb2d.gravityScale = 0;
            //rb2d.velocity = new Vector2(0,0);
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if(other.gameObject.tag == "Stair")
        {
            puedeSubirEscalera = false;
            rb2d.gravityScale = 10;
            HabilitarColisionConSuelo();
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.layer == 7 || other.gameObject.layer == 8)
        {
            puedeSaltar = true;
            contador = 0;
        }

        if(other.gameObject.tag == "Enemy" && star == false)
        {
            audioSource.PlayOneShot(AudioClips[2]);
            DisminuirVidaEn1();
            esIntangible = true;
        }

        if(other.gameObject.tag == "Enemy" && star == true)
        {
            audioSource.PlayOneShot(AudioClips[7]);
            //sr.enabled = false;
        }
        if(Life == 0)
        {
            audioSource.Pause();
            audioSource.PlayOneShot(AudioClips[4]);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "EnemyJump")
        {
            IncrementarPuntajeEn5();
        }
        if(other.gameObject.tag == "GoldCoin")
        {
            Destroy(other.gameObject);
            audioSource.PlayOneShot(AudioClips[3]);
            IncrementarPuntajeEn10();
        }
        if(other.gameObject.tag == "Star")
        {
            star = true;
            Destroy(other.gameObject);
            Crecer();
        }
    }
    
    private void setRunAnimation(){
       _animator.SetInteger("Estado",1);
    }
    
    private void setIdleAnimation(){
        _animator.SetInteger("Estado",0);
    }

    private void setSlideAnimation(){
        _animator.SetInteger("Estado",3);
    }
    
    private void setJumpAnimation(){
        _animator.SetInteger("Estado",2);
    }

    private void setDeadAnimation(){
        _animator.SetInteger("Estado",4);
    } 

    private void setExit()
    {
        _animator.SetInteger("Estado",5);
    }
    
    public void IncrementarPuntajeEn10()
    {
        Score += 10;
    }

    public void IncrementarPuntajeEn5()
    {
        Score += 5;
    }
    
    public void DisminuirVidaEn1()
    {
        Life--;
    }
}

