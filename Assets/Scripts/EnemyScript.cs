using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    private Rigidbody2D rb;
    private BoxCollider2D col;
    public GameObject eg;
    public Transform target;
    public float speed;
    private bool topa = false;
    // Start is called before the first frame update

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<BoxCollider2D>();
        InvokeRepeating("GenerarEnemigo", 2, 4);
    }

    // Update is called once per frame
    void Update()
    {
        if(topa == false)
        {
            rb.velocity = Vector2.left * 10;
        }
        if(topa == true)
        {
            SalirVolando();
        }
    }
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.name == "EnemiesClear")
            Destroy(this.gameObject);
        
        if(other.gameObject.layer == 9){
            //col.enabled = false;
            //rb.velocity = Vector2.down;
            rb.gravityScale = 0;

            topa = true;
            StartCoroutine("EliminarEnemigo");
        }
    }

    IEnumerator EliminarEnemigo()
    {
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
        topa = false;
    }

    private void SalirVolando()
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector2.MoveTowards(transform.position,target.position,step);
    }

    private void GenerarEnemigo()
    {
        var x = eg.transform.position.x;
        var y = eg.transform.position.y;
        Instantiate(rb, new Vector2(x,y), rb.transform.rotation);
    }
}
